package edu.sdq.dani882.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import edu.sdq.dani882.ui.internalFrames.RegistroPaciente;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;

	/**
	 * Create the frame.
	 */
	public Principal() {
		setTitle("Control de Pacientes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 647, 514);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panelNorte = new JPanel();
		contentPane.add(panelNorte, BorderLayout.NORTH);
		
		JButton btnRegistrarPaciente = new JButton("Registrar Paciente");
		btnRegistrarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				RegistroPaciente reg = new RegistroPaciente();
				desktopPane.add(reg);
				reg.setVisible(true);
				
			}
		});
		panelNorte.add(btnRegistrarPaciente);
		
		JButton btnBuscarPaciente = new JButton("Buscar Paciente");
		panelNorte.add(btnBuscarPaciente);
		
		JButton btnAyuda = new JButton("Ayuda");
		panelNorte.add(btnAyuda);
		
		JButton btnCerrarSeccion = new JButton("Cerrar Secci\u00F3n");
		panelNorte.add(btnCerrarSeccion);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
}
